package com.example.giuaky;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerViewStaffList;
    private ImageView btnAddPerson, btnSearch;

    private ArrayList<Staff> staffs = new ArrayList<>();
    private CustomRecyclerViewAdapter staffListAdapter = null;

    private DatabaseHelper myDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);

        myDatabase = new DatabaseHelper(this);
        myDatabase.createDB();
        myDatabase.openDB();

        staffs = myDatabase.getAllStaffs();

        recyclerViewStaffList = findViewById(R.id.staff_list);
        btnAddPerson = findViewById(R.id.btn_main_add_person);
        btnSearch = findViewById(R.id.btn_main_search);

        Intent intent = new Intent(this, Details.class);
        staffListAdapter = new CustomRecyclerViewAdapter(this, staffs, new StaffLongClickItemListener() {
            @Override
            public void onStaffClick(String staffID, int typeClick) {
                if(typeClick==STAFF_LONG_CLICK_REMOVE){
                    myDatabase.removeStaff(staffID);
                    updateList();
                }
                else if(typeClick==STAFF_LONG_CLICK_VIEW){
                    Toast.makeText(MainActivity.this, "Detail", Toast.LENGTH_SHORT).show();
                    intent.putExtra("staffID", staffID);
                    startActivity(intent);
                }
            }
        });

        recyclerViewStaffList.setAdapter(staffListAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewStaffList.setLayoutManager(linearLayoutManager);

        Intent toAddStaff = new Intent(this, AddStaff.class);
        ActivityResultLauncher<Intent> toAddStaffResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode() == Activity.RESULT_OK) {
                            updateList();
                        }
                    }
                });
        btnAddPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toAddStaffResultLauncher.launch(toAddStaff);
            }
        });

        Intent toSearch = new Intent(this, SearchStaff.class);
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(toSearch);
            }
        });

    }

    public void updateList(){
        staffs.clear();
        staffs = myDatabase.getAllStaffs();
        staffListAdapter.setNewData(staffs);
        staffListAdapter.notifyDataSetChanged();
        recyclerViewStaffList.setAdapter(staffListAdapter);
    }

}